test-unit:
	python test_gilded_rose.py


test-functional:
	python test_gilded_rose_functional.py


test: test-unit test-functional


coverage:
	coverage run test_gilded_rose.py
	coverage report --omit=test_gilded_rose.py -m
