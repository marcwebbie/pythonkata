# -*- coding: utf-8 -*-
import unittest

from gilded_rose import Item, GildedRose


class GuildedRoseTest(unittest.TestCase):

    def test_aged_brie_increases_quality_on_update(self):
        item = Item("Aged Brie", sell_in=2, quality=0)
        items = [item]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(1, items[0].quality)
        gilded_rose.update_quality()
        self.assertEqual(2, items[0].quality)

    def test_never_increases_quality_above_50(self):
        item = Item("Aged Brie", sell_in=100, quality=50)
        items = [item]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(50, items[0].quality)
        self.assertEqual(99, items[0].sell_in)

    def test_never_decreases_quality_bellow_0(self):
        item = Item("Common", sell_in=100, quality=0)
        items = [item]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(0, items[0].quality)

    def test_decreases_quality_by_1(self):
        item = Item("Common", sell_in=100, quality=1)
        items = [item]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(0, items[0].quality)

    def test_sulfuras_never_decreases_quality_on_update(self):
        item = Item("Sulfuras, Hand of Ragnaros", sell_in=0, quality=80)
        items = [item]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(80, items[0].quality)
        gilded_rose.update_quality()
        self.assertEqual(80, items[0].quality)

    def test_sulfuras_never_decreases_sell_in_on_update(self):
        item = Item("Sulfuras, Hand of Ragnaros", sell_in=10, quality=80)
        items = [item]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(10, items[0].sell_in)

    def test_backstage_pass_increases_1_in_quality_when_over_10_days(self):
        pass_over_10_days = Item("Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=10)
        items = [pass_over_10_days]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual(11, items[0].quality)

    def test_backstage_pass_increases_2_in_quality_when_less_than_10_days(self):
        pass_less_than_10_days = Item("Backstage passes to a TAFKAL80ETC concert", sell_in=9, quality=10)
        items = [pass_less_than_10_days]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(12, items[0].quality)

    def test_backstage_pass_increases_in_quality_when_less_than_5_days(self):
        pass_less_than_5_days = Item("Backstage passes to a TAFKAL80ETC concert", sell_in=4, quality=10)
        items = [pass_less_than_5_days]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(13, items[0].quality)

    def test_backstage_pass_does_not_increase_in_quality_over_50(self):
        pass_less_than_5_days = Item("Backstage passes to a TAFKAL80ETC concert", sell_in=4, quality=49)
        items = [pass_less_than_5_days]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(50, items[0].quality)

    def test_backstage_pass_is_zero_when_sellin_is_negative(self):
        pass_less_than_0_days = Item("Backstage passes to a TAFKAL80ETC concert", sell_in=-1, quality=10)
        items = [pass_less_than_0_days]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(0, items[0].quality)

    def test_conjured_degrades_twice_as_fast(self):
        mana_cake = Item("Conjured Mana Cake", sell_in=10, quality=10)
        items = [mana_cake]
        gilded_rose = GildedRose(items)

        gilded_rose.update_quality()
        self.assertEqual(8, items[0].quality)

    def test_str_repr(self):
        item = Item("Common", sell_in=100, quality=1)
        self.assertEqual("{}".format(item), "Common, 100, 1")


if __name__ == '__main__':
    unittest.main()
