# -*- coding: utf-8 -*-


def update_aged_brie(item):
    '''"Aged Brie" actually increases in Quality the older it gets'''
    item.sell_in -= 1
    item.quality = item.quality + 1 if item.quality < 50 else 50


def update_sulfura(item):
    '''"Sulfuras", being a legendary item, never has to be sold or decreases in Quality'''
    item.quality = 80
    return item


def update_backstage_pass(item):
    '''"Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
        Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
        Quality drops to 0 after the concert'''
    if item.sell_in < 0:
        item.quality = 0
    elif item.sell_in <= 5:
        item.quality = item.quality + 3 if item.quality < 48 else 50
    elif item.sell_in <= 10:
        item.quality = item.quality + 2 if item.quality < 49 else 50
    else:
        item.quality = item.quality + 1 if item.quality < 50 else 50
    item.sell_in -= 1
    return item


def update_conjured(item):
    '''"Conjured" items degrade in Quality twice as fast as normal items'''
    item.sell_in -= 1
    item.quality = item.quality - 2
    return item


def update_common(item):
    item.sell_in -= 1
    item.quality = item.quality - 1 if item.quality > 0 else 0
    return item


class GildedRose(object):

    def __init__(self, items):
        self.items = items

    def update_quality(self):
        for item in self.items:
            if item.name == "Aged Brie":
                update_aged_brie(item)
            elif item.name == "Sulfuras, Hand of Ragnaros":
                update_sulfura(item)
            elif item.name == "Conjured Mana Cake":
                update_conjured(item)
            elif item.name == "Backstage passes to a TAFKAL80ETC concert":
                update_backstage_pass(item)
            else:
                update_common(item)


class Item:

    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)
