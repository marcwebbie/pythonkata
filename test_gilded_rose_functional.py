import unittest

from gilded_rose import Item, GildedRose


class GuildedRoseFunctionalTest(unittest.TestCase):

    def test_3_days(self):
        items = [
            Item(name="+5 Dexterity Vest", sell_in=10, quality=20),
            Item(name="Aged Brie", sell_in=2, quality=0),
            Item(name="Elixir of the Mongoose", sell_in=5, quality=7),
            Item(name="Sulfuras, Hand of Ragnaros", sell_in=0, quality=80),
            Item(name="Sulfuras, Hand of Ragnaros", sell_in=-1, quality=80),
            Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20),
            Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=10, quality=49),
            Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=49),
            Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=2),
            Item(name="Conjured Mana Cake", sell_in=3, quality=6),  # <-- :O
        ]

        gilded_rose = GildedRose(items)

        for _ in ["day 1", "day 2", "day 3"]:
            gilded_rose.update_quality()

        # Item(name="+5 Dexterity Vest", sell_in=10, quality=20),
        self.assertEqual(7, gilded_rose.items[0].sell_in)
        self.assertEqual(17, gilded_rose.items[0].quality)

        # Item(name="Aged Brie", sell_in=2, quality=0),
        self.assertEqual(-1, gilded_rose.items[1].sell_in)
        self.assertEqual(3, gilded_rose.items[1].quality)

        # Item(name="Elixir of the Mongoose", sell_in=5, quality=7),
        self.assertEqual(2, gilded_rose.items[2].sell_in)
        self.assertEqual(4, gilded_rose.items[2].quality)

        # Item(name="Sulfuras, Hand of Ragnaros", sell_in=0, quality=80),
        self.assertEqual(0, gilded_rose.items[3].sell_in)
        self.assertEqual(80, gilded_rose.items[3].quality)

        # Item(name="Sulfuras, Hand of Ragnaros", sell_in=-1, quality=80),
        self.assertEqual(-1, gilded_rose.items[4].sell_in)
        self.assertEqual(80, gilded_rose.items[4].quality)

        # Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20),
        self.assertEqual(12, gilded_rose.items[5].sell_in)
        self.assertEqual(23, gilded_rose.items[5].quality)

        # Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=10, quality=49),
        self.assertEqual(7, gilded_rose.items[6].sell_in)
        self.assertEqual(50, gilded_rose.items[6].quality)

        # Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=49),
        self.assertEqual(2, gilded_rose.items[7].sell_in)
        self.assertEqual(50, gilded_rose.items[7].quality)

        # Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=49),
        self.assertEqual(2, gilded_rose.items[8].sell_in)
        self.assertEqual(11, gilded_rose.items[8].quality)

        # Item(name="Conjured Mana Cake", sell_in=3, quality=6),  # <-- :O
        self.assertEqual(0, gilded_rose.items[9].sell_in)
        self.assertEqual(0, gilded_rose.items[9].quality)

if __name__ == '__main__':
    unittest.main()
