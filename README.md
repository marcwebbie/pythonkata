# Kata Gilded Roses

## Running unit tests

```
make test-unit
```

## Running functional tests

```
make test-functional
```

## Running all tests

```
make test
```

## Running all unit tests with test coverage

```
pip install coverage
make coverage
```
